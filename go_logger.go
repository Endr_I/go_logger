package endrlogger

import (
	"errors"
	"log"
	"os"
	"runtime/debug"
)

type Config struct {
	ErrorLogFile string `json:"error_log_file"`
	DebugLogFile string `json:"debug_log_file"`
	InfoLogFile  string `json:"info_log_file"`
}

type Logger struct {
	defaultLogger *log.Logger
	errLogger     *log.Logger
	debugLogger   *log.Logger
	infoLogger    *log.Logger
	files         []*os.File
	config        *Config
}

func (logger *Logger) Error(msg ...interface{}) {
	if logger.errLogger != nil {
		logger.errLogger.Println(msg...)
	}
	logger.Info(msg...)
}

func (logger *Logger) Info(msg ...interface{}) {
	if logger.infoLogger != nil {
		logger.infoLogger.Println(msg...)
	}
	logger.Debug(msg...)
}

func (logger *Logger) Debug(msg ...interface{}) {
	if logger.debugLogger != nil {
		logger.debugLogger.Println(msg...)
		logger.debugLogger.Println(string(debug.Stack()))
	}
	logger.defaultLogger.Println(msg...)
}

func (logger *Logger) Close() {
	for _, file := range logger.files {
		file.Close()
	}
}

func (logger *Logger) GetConfig() *Config {
	return logger.config
}

func NewLogger(config *Config) *Logger {
	//if config.ErrorLogFile == "" {
	//	config.ErrorLogFile = "error.log"
	//}
	//if config.DebugLogFile == "" {
	//	config.DebugLogFile = "debug.log"
	//}
	defaultLogger := log.New(log.Writer(), "", log.LstdFlags)
	var files []*os.File
	errLogger, errFile := createLogger(config.ErrorLogFile, "ERROR: ")
	if errFile != nil {
		files = append(files, errFile)
	}
	debugLogger, debugFile := createLogger(config.DebugLogFile, "DEBUG: ")
	if debugFile != nil {
		files = append(files, debugFile)
	}
	infoLogger, infoFile := createLogger(config.InfoLogFile, "INFO: ")
	if infoFile != nil {
		files = append(files, infoFile)
	}
	return &Logger{
		defaultLogger: defaultLogger,
		errLogger:     errLogger,
		debugLogger:   debugLogger,
		infoLogger:    infoLogger,
		files:         files,
		config:        config,
	}
}

func createLogger(filePath string, prefix string) (*log.Logger, *os.File) {
	if filePath == "" {
		return log.New(log.Writer(), prefix, log.LstdFlags), nil
	}
	file, err := os.OpenFile(filePath, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		panic(errors.New("error opening file: " + err.Error()))
	}
	return log.New(file, prefix, log.LstdFlags), file
}
